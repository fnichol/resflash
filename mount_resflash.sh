#!/bin/sh

# Mount a resflash image or filesystem
# Copyright Brian Conway <bconway@rcesoftware.com>, see LICENSE for details

set -o errexit -o nounset -o pipefail #-o xtrace # DEBUG

. $(dirname ${0})/resflash.sub

if [ ${#} -ne 1 ]; then
  echo "Usage: ${0} resflash_img_or_fs"
  exit 1
fi

mount_img_or_fs ${1}
echo ${MNTPATH}
